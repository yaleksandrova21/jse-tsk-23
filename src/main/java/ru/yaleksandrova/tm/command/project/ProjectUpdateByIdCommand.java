package ru.yaleksandrova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractProjectCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER ID:");
        @NotNull final String id = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        System.out.println("ENTER NAME:");
        @NotNull final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = ApplicationUtil.nextLine();
        final Project projectUpdatedId = serviceLocator.getProjectService().updateById(userId, id, name, description);
        if (projectUpdatedId == null) {
            return;
        }
        System.out.println("[Project updated]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
