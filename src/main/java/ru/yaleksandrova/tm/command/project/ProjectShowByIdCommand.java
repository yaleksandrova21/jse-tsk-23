package ru.yaleksandrova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractProjectCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER ID]");
        @NotNull final String id = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, id);
        if (project==null){
            throw new ProjectNotFoundException();
        }
        showProject(project);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
