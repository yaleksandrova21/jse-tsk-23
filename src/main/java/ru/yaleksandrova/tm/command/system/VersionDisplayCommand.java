package ru.yaleksandrova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.command.AbstractCommand;

public final class VersionDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
