package ru.yaleksandrova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER ID:");
        @NotNull final String id = ApplicationUtil.nextLine();
        final Task task  = serviceLocator.getTaskService().findById(userId, id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        System.out.println("ENTER NAME:");
        @NotNull final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = ApplicationUtil.nextLine();
        final Task taskUpdatedId = serviceLocator.getTaskService().updateById(userId, id, name, description);
        if (taskUpdatedId == null) {
            return;
        }
        System.out.println("[Task updated]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
