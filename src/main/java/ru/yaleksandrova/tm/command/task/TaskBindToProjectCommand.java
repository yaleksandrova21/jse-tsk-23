package ru.yaleksandrova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-bind-to-project";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskId = ApplicationUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectId = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        final Task taskUpdated = serviceLocator.getProjectTaskService().bindTaskById(userId, projectId, taskId);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
