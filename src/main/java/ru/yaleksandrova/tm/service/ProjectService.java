package ru.yaleksandrova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.api.sevice.IProjectService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.empty.EmptyNameException;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;

import java.util.Collections;
import java.util.List;
import java.util.Comparator;


public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        projectRepository.add(project);
    }

    @NotNull
    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @NotNull
    @Override
    public Project findByName(@Nullable final String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(@Nullable final String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = findById(userId, id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = findByIndex(userId, index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeByName(@Nullable String userId, @Nullable String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public Project startByIndex(@Nullable String userId, @Nullable Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return projectRepository.startByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project startByName(@Nullable String userId, @Nullable String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @NotNull
    @Override
    public Project startById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @NotNull
    @Override
    public Project finishById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.finishById(userId, id);
    }

    @NotNull
    @Override
    public Project finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return projectRepository.finishByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project finishByName(@Nullable String userId, @Nullable String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @NotNull
    @Override
    public Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) return null;
        return projectRepository.changeStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) return null;
        return projectRepository.changeStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public Project changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) return null;
        return projectRepository.changeStatusByName(userId, name, status);
    }

    @NotNull
    @Override
    public Project removeById(@Nullable String userId, @Nullable String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        projectRepository.remove(project);
        return project;
    }

    @Override
    public Integer size(String userId) {
        return projectRepository.size();
    }

}
