package ru.yaleksandrova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArg();

    @Nullable
    AbstractCommand getCommandByName(String name);

    @Nullable
    AbstractCommand getCommandByArg(String arg);

    void add(@NotNull AbstractCommand command);


}
